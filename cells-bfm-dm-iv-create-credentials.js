class CellsBfmDmIvCreateCredentials extends Polymer.Element {

  static get is() {
    return 'cells-bfm-dm-iv-create-credentials';
  }

  static get properties() {
    return {
      endpoint: String,
      headers: {
        type: Object,
        value: () => ({})
      },
      body: {
        type: Object,
        value: () => ({})
      },
      host: {
        type: String,
        value: () => ''
      }
    };
  }
  /**
   * Function to handle service success response
   * @param {*} response
   */
  _successResponse(response) {
    let jobId = response.detail.response.id_job;
    this.dispatchEvent(new CustomEvent('validate-credential-status', {
      detail: jobId
    }));
    this.dispatchEvent(new CustomEvent('response-create-credential', {
      detail: response.detail.response
    }));
  }
  /**
   * Function to generate enpoint and call service
   */
  starterMethod(credentials) {
    this._buildEndpoint();
    this._buildBody(credentials);
    if (this.endpoint) {
      this.dispatchEvent(new CustomEvent('send-status-pending-data',{
        detail: {
          headIlustration: {
            type: 'spinner',
          },
          link: {
            event: 'link-cancel',
            text: 'Cancelar',
            type: 'link',
          },
          title: 'Agregando sus facturas',
          detail: 'Estamos obteniendo la información de las facturas de la empresa registrada EMPRESA ALPHA SA de CV de su contrato BBVA Net Cash.',
          info: 'El proceso puede tardar de 1 a 7 minutos en completarse.',
          aditional: 'Si cancela o cierra la ventana tendrá que inicar el proceso nuevamente.'
        }
      }));


      this.dispatchEvent(new CustomEvent('manage-display-message-status', {
        detail: false,
      }));
      this.$.dpCreateCredentials.generateRequest();
    }
  }
  _buildBody(credentials) {
    this.set('body', JSON.parse(JSON.stringify(credentials)));
  }
  /**
   * function to buil enpoint url
   */
  _buildEndpoint() {
    let host = this._getHost();
    let isMock = this._getMocks();
    this.set('endpoint', `${host}/credentials_post${isMock}`);
  }
  _getMocks(){
    return (window.AppConfig && window.AppConfig.mocks) ? '.json' : '';
  }
  /**
   * Function to get host from config file
   */
  _getHost() {
    return (window.AppConfig && window.AppConfig.host) ? window.AppConfig.host : this.host;
  }
  /**
   * Function to hanlde error status response
   * @param {*} response
   */
  _handlerError(response) {
    var msj = 'Estamos teniendo dificultades técnicas, favor de intentar mas tarde';
    try {
      var errorCode = response.detail.code;
      switch (errorCode) {
        case 401:
          this.dispatchEvent(new CustomEvent('unauthorized-error', { detail: response.detail.code }));
          break;
        default:
          msj = response.detail['error-message'] || errorCode || msj;
          break;
      }
    } catch (e) {
      //No aplicable catch handler, use default values from eventError and msj
    }
    this.dispatchEvent(new CustomEvent('hidde-spinner',{detail: true}))
    this.dispatchEvent(new CustomEvent('error-add-credentials'));
    this.dispatchEvent(new CustomEvent('error-service', { detail: msj }));
  }
}

customElements.define(CellsBfmDmIvCreateCredentials.is, CellsBfmDmIvCreateCredentials);
